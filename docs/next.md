---
image: https://opsindev.news/assets/images/FIXME.png
---

# YYYY-MM-DD - NEXT

**Everything is a draft. Everyone can [contribute](https://opsindev.news/contributing/).**

Tasks:

1. Prepare the next newsletter
2. Upload an image into docs/assets/images and update the YAML header with the full URL, for social previews. 
3. Verify the social preview with [opengraph.xyz](https://opengraph.xyz) 
4. Prepare publishing, and follow social media CTAs with threads.
5. After publishing, empty this file's content, and update the image header to `FIXME.png`. 

End CTA on social:

```
👋 🌱 🐝 🤖 🛡️⛅ 👁️ 📚 🔖 🎯 🎥 🎤

Love to learn together, and follow the Ops in Dev newsletter?

Subscribe at https://buttondown.email/opsindev.news 

#Observability #DevSecOps #CloudNative #AI #eBPF 
```

Social CTA snippets:

```
👁️ More #observability insights in my Ops In Dev newsletter opsindev.news - subscribe at https://buttondown.email/opsindev.news  #learntogether 
🛡️ More #security insights in my Ops In Dev newsletter opsindev.news - subscribe at https://buttondown.email/opsindev.news 💡 #learntogether 
⛅ More #cloudnative insights in my Ops In Dev newsletter opsindev.news - subscribe at https://buttondown.email/opsindev.news 💡 #learntogether 
🤖 More #machinelearning insights in my Ops In Dev newsletter opsindev.news - subscribe at https://buttondown.email/opsindev.news 💡 #learntogether 
🎯 More release speed-run updates in my Ops In Dev newsletter opsindev.news - subscribe at https://buttondown.email/opsindev.news 💡 #learntogether 
📚 More tools & tips for daily use in my Ops In Dev newsletter opsindev.news - subscribe at https://buttondown.email/opsindev.news 💡 #learntogether 
Subscribe at https://buttondown.email/opsindev.news to learn #observability #DevSecOps #machinelearning including practical tips and tools 💡  
```

Resources 

- Tools: https://dev.to/deepu105/rust-easy-modern-cross-platform-command-line-tools-to-supercharge-your-terminal-4dd3
  - https://sysdig.com/blog/top-15-kubectl-plugins-for-security-engineers/ 
  - https://github.com/collabnix/kubetools 
  - https://github.com/sbilly/awesome-security 
- Releases:
    - Prometheus: https://github.com/prometheus/prometheus/releases Parca: https://github.com/parca-dev/parca/releases Pyrra: https://github.com/pyrra-dev/pyrra/releases Zabbix https://www.zabbix.com/release_notes Perses https://github.com/perses/perses/releases Coroot: https://github.com/coroot/coroot/releases Parca Agent: https://github.com/parca-dev/parca-agent/releases
    - Flux https://github.com/fluxcd/flux2/releases Cilium https://github.com/cilium/cilium/releases tracee https://github.com/aquasecurity/tracee/releases https://twitter.com/AquaTracee/status/1666465487310471172  Kyverno https://nirmata.com/blog/ OPA https://github.com/open-policy-agent/opa/releases Chaos Mesh https://chaos-mesh.org/versions/ Keptn https://github.com/keptn/keptn/releases 
    - Kubernetes https://kubernetes.io/blog/ Rust: https://blog.rust-lang.org/ Hashicorp https://www.hashicorp.com/blog/categories/products-technology 



## Future content

# 2024-03-xx

_Thanks for reading the web version, you can subscribe to the [Ops In Dev newsletter](https://buttondown.email/opsindev.news) to receive it in your mail inbox._

<iframe
scrolling="no"
style="width:100%!important;height:220px;border:1px #ccc solid !important"
src="https://buttondown.email/opsindev.news?as_embed=true"
></iframe><br /><br />

<!-- Copy everything below into the Buttondown newsletter form. -->

## 👋 Hey, lovely to see you again



### 🤖 The Inner Dev learning AI/ML 



Quick wins:

- 

### 🐝 The Inner Dev learning eBPF


Quick wins: 

- 


## 👁️ Observability



Quick wins:

- 

## 🛡️ DevSecOps 


Quick wins:

- 

## 🌤️ Cloud Native



Quick wins:

- 


## 📚 Tools and tips for your daily use

- 

AI/ML focussed tools:

- 

## 🔖 Book'mark

- Book: [DevOps - Wie IT-Projekte mit einem modernen Toolset und der richtigen Kultur gelingen](https://www.rheinwerk-verlag.de/devops/) (German) by Sujeevan Vijayakumaran, published March 2024 


## 🎯 Release speed-run

- 

## 🎥 Events and CFPs

- Mar 15: [DevOpsDays LA](https://devopsdays.org/events/2024-los-angeles/welcome/) in Pasadena, CA. 
- Mar 18-20: [SRECON Americas](https://www.usenix.org/conference/srecon24americas) in San Francisco, CA. 
- Mar 17-18: [Cloud Native Rejekts](https://cloud-native.rejekts.io/) in Paris, France. 
- Mar 19-22: [KubeCon EU 2024](https://events.linuxfoundation.org/kubecon-cloudnativecon-europe/) in Paris, France. 
- Apr 8-10: [QCon London](https://qconlondon.com/) in London, UK.
- Apr 16-17: [DevOpsDays Zurich](https://devopsdays.org/events/2024-zurich/welcome/) in Winterthur, Switzerland. 
- Apr 16-18: [Open Source Summit NA 2024](https://events.linuxfoundation.org/open-source-summit-north-america/) in Seattle, Washington. 
- May 7: [KubeHuddle Toronto 2024](https://ca.kubehuddle.com/) in Toronto, Canada. 
- May 14-15: [DevOpsDays Seattle](https://devopsdays.org/events/2024-seattle/welcome/) in Seattle, WA.
- Jun 10-12: [Monitorama 2024](https://monitorama.com/2024/pdx.html) in Portland, OR. 
- Jun 17-21: [Cloudland 2024](https://www.cloudland.org/en/home/) in Phantasialand near Cologne, Germany. 
- Jun 19-21: [DevOpsDays Amsterdam](https://devopsdays.org/events/2024-amsterdam/welcome/) in Amsterdam, The Netherlands. 
- Jun 20: [Kubernetes Community Days Italy](https://community.cncf.io/events/details/cncf-kcd-italy-presents-kcd-italy-2024/) in Bologna, Italy. 
- Jul 1-2: [Kubernetes Community Days Munich 2024](https://www.kcdmunich.com/) in Munich, Germany. 
- Sep 3-4: [Container Days 2024](https://www.containerdays.io/containerdays-conference-2024/) in Hamburg, Germany.
- Sep 16-18: [Open Source Summit EU 2024](https://events.linuxfoundation.org/open-source-summit-europe/) in Vienna, Austria. 
- Sep 26-27: [DevOpsDays London](https://devopsdays.org/events/2024-london/welcome/) in London, UK. 
- Nov 12-15: [KubeCon NA 2024](https://events.linuxfoundation.org/kubecon-cloudnativecon-north-america-2024/) in Salt Lake City, Utah. 

👋 CFPs due soon 

- May 7: [KubeHuddle Toronto 2024](https://ca.kubehuddle.com/) in Toronto, Canada. [CFP](https://ca.kubehuddle.com/#call-for-proposals) closes Feb 9. 
- Jul 1-2: [Kubernetes Community Days Munich 2024](https://www.kcdmunich.com/) in Munich, Germany. [CFP](https://sessionize.com/kcd-munich-2024-cfp/) closes Mar 31. 
- Sep 3-4: [Container Days 2024](https://www.containerdays.io/containerdays-conference-2024/) in Hamburg, Germany. [CFP](https://www.containerdays.io/containerdays-conference-2024/call-for-papers/) closes Mar 31. 

_Looking for more CfPs?_ 

- [Developers Conferences Agenda](https://developers.events/) by Aurélie Vache.
- [Kube Events](https://kube.events/call-for-papers).
- [GitLab Speaking Resources handbook](https://about.gitlab.com/handbook/marketing/corporate-communications/speaking-resources/).


## 🎤 Shoutouts 



🌐 

Thanks for reading! If you are viewing the website archive, make sure to [subscribe](https://buttondown.email/opsindev.news) to stay in the loop! See you next month  🤗 

Cheers,
Michael 

PS: If you want to share items for the next newsletter, just reply to this newsletter, [send a merge request](https://gitlab.com/dnsmichi/opsindev.news/blob/main/docs/next.md), or let me know through [LinkedIn](https://www.linkedin.com/in/dnsmichi/), [Twitter/X](https://twitter.com/dnsmichi), [Mastodon](https://crashloop.social/@dnsmichi), [Blue Sky](https://bsky.app/profile/dnsmichi.bsky.social). Thanks! 
