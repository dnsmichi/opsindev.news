# Template

Copy the structure into `docs/next.md`.

```
## 👋 Hey, nice to meet you 



## ☕ Hot Topics



### 👁️ Observability



### 🔥 Chaos Engineering



### 🛡️ The Sec in Ops in Dev 



### 🔍 The inner Dev



## 📈 Your next project could be ...



## 📚 Tools and tips for your daily use



## 🎥 Events and CfPs



## 🎤 Shoutouts 




Thanks for reading! See you next time - monthly or bi-weekly, let me know what you think on [Twitter](https://twitter.com/dnsmichi) or [LinkedIn](https://www.linkedin.com/in/dnsmichi/).

Cheers,
Michael 

PS: If you want to share items for the next newsletter, please check out the [contributing guide](https://opsindev.news/contributing/). Thanks! 
```