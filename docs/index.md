# opsindev.news 

> **NOTE**
> 
> Public archive since 2024-03. 
> I changed my focus areas and time commitments and will continue sharing my learnings on AI, Embedded DevSecOps and more on my personal blog [dnsmichi.at](https://dnsmichi.at/), [LinkedIn](https://www.linkedin.com/in/dnsmichi/) and [Bluesky](https://bsky.app/profile/dnsmichi.dev).

## 👋 Hey, nice to meet you

From Dev to Ops to DevSecOps to SRE, learning the value of Observability. Interested in solving engineering challenges, and day-2-ops with ML/AI? Welcome to my story; you are invited to learn together :)

I started as a backend developer in an OSS monitoring project, learned how the software was used in Ops, and later appreciated the performance insights and tools to become a better developer. I see myself as the Ops in Dev, or a developer turned ops, with all mistakes on the way. I have adopted cloud-native microservice architectures and set goals to learn new technology (eBPF, OpenTelemetry, AI/ML, etc.), and educate everyone. Join my learning experience by subscribing to the Ops In Dev newsletter! 

## 💡 How to get the newsletter

### More Options 

The newsletter is organized in [https://gitlab.com/dnsmichi/opsindev.news/](https://gitlab.com/dnsmichi/opsindev.news/), transparent and public so that everyone can contribute.

> _**Note**_
>
> _This newsletter solely reflects the personal opinion of [Michael Friedrich](https://dnsmichi.at/), and not those of his employer [GitLab](https://about.gitlab.com/)._
