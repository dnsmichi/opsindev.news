---
image: https://opsindev.news/assets/images/lm_studio_llama2_chat_cpp_design_patterns.png
---

# 2024-02-08: You Choose, LM Studio, WebAssembly and LLMs, Telepresence, Elastic profiling donation, Cilium 1.15

_Thanks for reading the web version, you can subscribe to the [Ops In Dev newsletter](https://buttondown.email/opsindev.news) to receive it in your mail inbox._

<iframe
scrolling="no"
style="width:100%!important;height:220px;border:1px #ccc solid !important"
src="https://buttondown.email/opsindev.news?as_embed=true"
></iframe><br /><br />

<!-- Copy everything below into the Buttondown newsletter form. -->

## 👋 Hey, lovely to see you again

2024 brought new possibilities to run LLMs locally and learn how cloud-native technology (WebAssembly, Kubernetes, etc.) drives AI innovation. Besides learning new technology, this newsletter issue brings practical tools and tips for Dev, Sec and Ops. And if you prefer video content for learning, my new favorite is "You Choose" (more below in the Cloud Native section). Enjoy reading! 

### 🤖 The Inner Dev learning AI/ML 

[LM Studio](https://lmstudio.ai/) allows you to discover, download and run local LLMs. Thanks, [Sid Sijbrandij](https://www.linkedin.com/in/sijbrandij/), for sharing in the [January CEO AMA on GitLab Unfiltered](https://www.youtube.com/watch?v=YJFtXkfpyS4&t=670s). Next to the integrated chat UI, LM Studio also supports running a local API, where an OpenAI SDK library or compatible can interact programmatically. [Ollama](https://ollama.ai/) provides similar functionality. 

The LM Studio screenshot shows a question I had in 2003, failing to understand the producer-consumer design pattern in C++ and how it relates to multithreading with shared resources: `Please explain the producer-consumer design pattern in C++, and show examples with multithreading OOP.` It uses the Meta LLama 2 model.

![LM Studio chat prompt, LLama 2, prompt with C++ questions and explanations](https://opsindev.news/assets/images/lm_studio_llama2_chat_cpp_design_patterns.png)

The full output is available in the [web version of this newsletter](https://opsindev.news/archive/2024-02-08/). 

---
<details>
<summary>
Full LLama 2 response
</summary>

The Producer-Consumer Design Pattern:
The Producer-Consumer design pattern is a solution to the problem of managing shared resources between two or more components. It involves separating the components that produce and consume these resources into separate classes, allowing them to operate independently without interfering with each other. This pattern is useful when there are multiple threads accessing shared data in a multi-threaded environment.

In C++, the Producer-Consumer pattern can be implemented using the following steps:

1. Define the interface for the resource producer and consumer classes. The producer class should provide the resource to the consumer class, and the consumer class should consume the resource provided by the producer class.
2. Implement the producer and consumer classes independently without interfering with each other. This can be achieved by using separate threads or processes, or by using synchronization mechanisms such as mutexes or semaphores to coordinate access to the shared resource.
3. Use a buffer or queue to store the resources produced by the producer class until they are consumed by the consumer class. This ensures that the producer and consumer classes operate independently without interfering with each other.
4. Implement the necessary synchronization mechanisms to coordinate access to the shared resource between the producer and consumer classes. This can include using mutexes, semaphores, or other synchronization primitives to ensure that only one class can access the resource at a time.
Examples with Multithreading OOP:
1. Producer-Consumer Example with Mutex:

```
#include <iostream>
#include <mutex>

class Producer {
public:
    void produce(int* data) {
        // Produce data
        std::lock_guard<std::mutex> lock(mutex);
        consumer.consume(data);
    }
};
class Consumer {
public:
    void consume(int data) {
        // Consume data
        std::lock_guard<std::mutex> lock(mutex);
    }
private:
    std::mutex mutex;
    Producer producer;
};
int main() {
    Producer producer;
    Consumer consumer;

    // Start the producer thread
    std::thread producerThread(&Producer::produce, &producer, new int[10]);

    // Start the consumer thread
    std::thread consumerThread(&Consumer::consume, &consumer);

    // Wait for the producer to finish producing data
    producerThread.join();

    return 0;
```

In this example, a mutex is used to synchronize access to the shared resource between the producer and consumer classes. The producer class produces the data, and the consumer class consumes it. The mutex ensures that only one class can access the resource at a time, preventing race conditions and other synchronization issues.
2. Producer-Consumer Example with Condition Variable:

```
#include <iostream>
#include <condition_variable>

class Producer {
public:
    void produce(int* data) {
        // Produce data
        std::cv_wait(&cv, &mutex);
        consumer.consume(data);
    }
};
class Consumer {
public:
    void consume(int data) {
        // Consume data
        std::cv_wait(&cv, &mutex);
    }
private:
    std::condition_variable cv;
    std::mutex mutex;
    Producer producer;
};
int main() {
    Producer producer;
    Consumer consumer;

    // Start the producer thread
    std::thread producerThread(&Producer::produce, &producer, new int[10]);

    // Start the consumer thread
    std::thread consumerThread(&Consumer::consume, &consumer);

    // Wait for the producer to finish producing data
    producerThread.join();

    return 0;
```

In this example, a condition variable is used to synchronize access to the shared resource between the producer and consumer classes. The producer class produces the data, and the consumer class consumes it. The condition variable ensures that only one class can access the resource at a time, preventing race conditions and other synchronization issues.
Conclusion:
The Producer-Consumer design pattern is an effective solution to managing shared resources between multiple components in a multi-threaded environment. By separating the producer and consumer classes into separate threads or processes, and using synchronization mechanisms such as mutexes or condition variables, we can ensure that the resource is accessed safely and efficiently. This pattern is widely used in many applications, including databases, file systems, and network protocols.

</details>
---

[vLLM](https://blog.vllm.ai/2023/06/20/vllm.html) is a new open-source library for fast LLM inference and serving. It uses PagedAttention, a new attention algorithm that efficiently manages attention keys and values, which delivers up to 24x higher throughput than HuggingFace Transformers. Thanks to [Priyanka Sharma](https://www.linkedin.com/posts/pritianka_vllm-easy-fast-and-cheap-llm-serving-with-activity-7158716056327385088-2-Qj?utm_source=share&utm_medium=member_desktop) for sharing. 

Code generation with AI can be different to code completion tasks. When using different large language models for each task, the IDE integration needs to quickly analyze and identify the scope of the typed source code. I've met with [Erran Carey](https://www.linkedin.com/in/errancarey/) for [a coffee chat to learn how the GitLab Language Server for IDEs is built](https://www.youtube.com/watch?v=VQlWz6GZhrs), and how it uses WebAssembly with TreeSitter for language parsing. This helps with intent detection for either code completion or [code generation with Anthropic](https://about.gitlab.com/blog/2024/01/16/gitlab-uses-anthropic-for-smart-safe-ai-assisted-code-generation/). You can learn more about language models used by GitLab [here](https://docs.gitlab.com/ee/user/ai_features.html#language-models), and follow the new [GitLab Duo Coffee Chat series on YouTube](https://www.youtube.com/playlist?list=PL05JrBw4t0Kp5uj_JgQiSvHw1jQu0mSVZ).

Ollama announced [Python and Javascript libraries](https://ollama.ai/blog/python-javascript-libraries), making communicating and developing integrations easier, for example, with a Python library example for refining training data (source: [gist](https://gist.github.com/ivanfioravanti/bcacc48ef68b02e9b7a4034161824287), [Twitter/X](https://twitter.com/ivanfioravanti/status/1750309466174640465?s=12)). [Ollama Vision](https://ollama.ai/blog/vision-models) brings support for open-source multimodal models (source: [Twitter/X](https://twitter.com/ollama/status/1753530905069748506?s=12)). 

Quick wins:

- [Intro to Large Language Models (1 hour talk)](https://www.youtube.com/watch?v=zjkBMFhNj_g)
- [Podcast: Open Source LLMs with Simon Willison](https://oxide.computer/podcasts/oxide-and-friends/1692510)
- [Talk: What's wrong with LLMs and what we should be building instead](https://www.youtube.com/watch?v=cEyHsMzbZBs) by Tom Dietterich
- [Mixing small language models and large ones for faster learning](https://youtu.be/GaaQaCoNpU4)
- [Personalizing AI – How to Build Apps Based on Your Own Data](https://whiteduck.de/personalizing-ai-how-to-build-apps-based-on-your-own-data/) by [Nico Meisenzahl](https://www.linkedin.com/in/nicomeisenzahl/)
- A European OpenAI competitor is founded with NXAI by Sepp Hochreiter with Austrian industry partners and the University of Linz (source: [heise.de (German)](https://www.heise.de/news/NXAI-Sepp-Hochreiter-will-europaeische-Antwort-auf-OpenAI-erschaffen-9618232.html)). Hochreiter developed the [Long Short-Term Memory (LSTM) algorithm](https://en.wikipedia.org/wiki/Long_short-term_memory) which helped improve AI applications in the 90ies. 

### 🐝 The Inner Dev learning eBPF

[Dan](https://twitter.com/thebsdbox) wrote a DNS packet parser for an eBPF program ([example source](https://github.com/thebsdbox/learning-ebpf/blob/main/ebpf/dns/dns.c)), which allows manipulating DNS requests on the Kernel level. This could allow the inspection of DNS queries more efficiently and open doors for chaos engineering experiments with eBPF.

eBPF can also be used for penetration testing tools. [eBPFeXPLOIT](https://github.com/bfengj/eBPFeXPLOIT) supports hiding PIDs, MemoryShell (injecting code into memory), block the kill command, hide injected eBPF programs, SSH and cron backdoors, etc. Many things I never heard of -- sounds frightening if this is used by malicious actors instead. 

Quick wins: 

- [Monitoring Dynamic Linker Hijacking With eBPF](https://dzone.com/articles/monitoring-dynamic-linker-hijacking-with-ebpf)
- The eBPF foundation [published the "State of eBPF" report](https://ebpf.foundation/report-the-state-of-ebpf/). 


## 👁️ Observability

Elastic proposed to donate their continuous profiling agent to the OpenTelemetry CNCF project ([issue](https://github.com/open-telemetry/community/issues/1918)). The technical committee is discussing the required steps, including licensing and reviewing the currently closed source. When this donation hopefully is fruitful, everyone can benefit from and contribute to [continuous profiling](https://o11y.love/topics/profiling/). The Parca maintainers already [joined the discussion](https://github.com/open-telemetry/community/issues/1918#issuecomment-1917581404) to maybe merge the efforts into a unified OpenTelemetry profiling agent.

[Coroot 0.26 supports effortless .NET runtime monitoring](https://github.com/coroot/coroot/releases/tag/v0.26.0), without additional configuration. The metrics are collected with the [Coroot node agent](https://github.com/coroot/coroot-node-agent/blob/54b2a3d3e7294dae9f780967cf924aa0b3128961/containers/dotnet.go), using a [fork of the pyroscope-io/dotnetdiag library](https://github.com/coroot/dotnetdiag). 

Quick wins:

- [Monitoring TLS Endpoint Certificate Expiration with Prometheus](https://promlabs.com/blog/2024/02/06/monitoring-tls-endpoint-certificate-expiration-with-prometheus/)
- [What you need to know before creating your first OpenTelemetry pipeline for tracing](https://medium.com/adidoescode/what-you-need-to-know-before-creating-your-first-opentelemetry-pipeline-for-tracing-9d7838514cb9)


## 🛡️ DevSecOps 

If you are using a Fritzbox router from AVM, beware of using the local `fritz.box` domain. After the gTLD `.box` became available, someone registered the domain and serves NFTs (source: [heise.de (German)](https://www.heise.de/news/Verwirrend-Internet-Domain-fritz-box-zeigt-NFT-Galerie-statt-Router-Verwaltung-9610149.html)). It could also become a target of phishing attacks, simulating a login portal form on `fritz.box` for example.

Google [announced an Android emulator and iOS simulator right in the browser](https://idx.dev/blog/article/introducing-android-emulators-ios-simulators-and-other-product-updates-from), through their Project IDX. 

[Chromium will support Rust](https://security.googleblog.com/2023/01/supporting-use-of-rust-in-chromium.html), when third-party libraries are called from the C++ source code. Interop is limited to a single direction from C++, though. Google also announced a grant of $1 million to the Rust Foundation to help [improve the interoperability between Rust and C++](https://security.googleblog.com/2024/02/improving-interoperability-between-rust-and-c.html).

Zed is a new lightweight IDE and [fully open-source](https://zed.dev/blog/zed-is-now-open-source). It is currently only available on macOS and continues to add support for more languages, etc., in the latest [0.121.5 release](https://zed.dev/releases/stable/0.121.5).

Quick wins:

- [Measuring Developer Productivity: Real-World Examples](https://open.substack.com/pub/pragmaticengineer/p/measuring-developer-productivity-bae?r=1byu3n&utm_campaign=post&utm_medium=email) is a deep dive by [Gergely Orosz](https://www.linkedin.com/in/gergelyorosz/) and [Abi Noda](https://www.linkedin.com/in/abinoda/) into developer productivity metrics used by Google, LinkedIn, Peloton, Amplitude, Intercom, Notion, Postman, and 10 other tech companies.
- [The Unique Role of DevOps, SRE, and Platform Engineering in Different Organizations](https://mccricardo.com/the-unique-role-of-devops-sre-and-platform-engineering-in-different-organizations/)
- [Mastering Kubernetes security: Safeguarding your container kingdom](https://redcanary.com/blog/kubernetes-security/)

## 🌤️ Cloud Native

[Telepresence](https://www.telepresence.io/) is a CNCF sandbox project that allows you to intercept service/namespace traffic in remote Kubernetes clusters and tunnel it to your local machine. This method can help with development and testing and does not require additional ingress configuration. I learned about Telepresence while helping review a blog post [how Telepresence works with cloud development environments in Gitpod](https://www.gitpod.io/guides/using-telepresence-in-gitpod).  

[WebAssembly System Interface (WASI) 0.2](https://blog.sunfishcode.online/wasi-0-2/) is now based on the [WebAssembly component model](https://github.com/WebAssembly/component-model/) and provides a stable API for library developers. This is a huge step forward in improving the developer experience with WebAssembly, where higher-level libraries are needed for wider adoption. 

The talk [WebAssembly is becoming the runtime for LLMs](https://www.youtube.com/watch?v=upNNI_b4tNY) shares an interesting shift from LLM apps in Python:

> Today’s LLM apps, including inference apps and agents, are mostly written in Python. But this is about to change. Python is too slow, too bloated, and too complicated to install and manage. That’s why popular LLM frameworks, such as llama2.c, whisper.cpp, llama.rs, all thrive to have zero Python dependency. All those post-Python LLM applications and frameworks are written in compiled languages (C/C++/Rust) and can be compiled into Wasm.

At Rejekts NA 2023, I learned about the amazing series [You Choose](https://www.youtube.com/playlist?list=PLyicRj904Z9-FzCPvGpVHgRQVYJpVmx3Z), where [Viktor Farcic](https://www.linkedin.com/in/viktorfarcic/) and [Whitney Lee](https://www.linkedin.com/in/whitneylee/) explain cloud-native concepts and let the audience decide interactively which CNCF projects to use in their walkthroughs. The Rejekts talk ["Choose Your Own Adventure: The Perilous Passage to Production"](https://www.youtube.com/watch?v=3P33bFK87gw) is a great introduction about what to expect. If you want to jump right in, the latest episode is about [Secrets Management](https://www.youtube.com/watch?v=rpNFuT4TCQY). 

Quick wins:

- [How Kubernetes picks which pods to delete during scale-in](https://rpadovani.com/k8s-algorithm-pick-pod-scale-in)
- [What is Kube-Proxy and why move from iptables to eBPF? ](https://isovalent.com/blog/post/why-replace-iptables-with-ebpf/)
- Docker vs. containerd vs. Podman - [a learning thread on Twitter/X](https://twitter.com/iximiuz/status/1751674025191875064?s=12). 


## 📚 Tools and tips for your daily use

- [Building container images from scratch (with the scratch base image)](https://medium.com/@m0v_3r/building-container-images-from-scratch-with-the-scratch-base-image-8ff56812124b)
- [How to Reverse-Proxy Applications on Subpaths with Traefik](https://dbodky.me/blog/host-on-subpath-with-traefik/)
- [Debugging Distroless Containers with Docker Debug](https://www.youtube.com/watch?v=ELxIBB2Uy2E)
- [Quickemu](https://github.com/quickemu-project/quickemu) allows to quickly create and run optimised Windows, macOS and Linux desktop virtual machines.
- [Ksctl](https://github.com/kubesimplify/ksctl) by KubeSimplify is a cloud agnostic Kubernetes management tool that thelps manage cluster running in multi-cloud environments. 
- [Zen](https://github.com/anfragment/zen) is a simple, free and efficient ad-blocker and privacy guard for Windows, macOS and Linux. 
- Chrome Developer Tools Tips: Hold shift while hovering over a request, and it highlights the initiator in green, and dependencies in red. Thanks [Addy Osmani](https://www.linkedin.com/posts/addyosmani_developers-programming-devtools-activity-7152562617918607360-8qBu?utm_source=share&utm_medium=member_desktop). 
- Slack tip: When you prefix an emoji with `+`, you can react to the previous message. Thanks [Thomas Paul Mann](https://twitter.com/thomaspaulmann/status/1746923308694503482?s=12).

AI/ML focussed tools:

- [LlamaIndex](https://docs.llamaindex.ai/en/stable/) is a data framework for LLM-based applications to ingest, structure, and access private or domain-specific data.
- [E2B](https://e2b.dev/docs) is a sandbox for AI apps and agents. It provides a long-running cloud environment to run any LLM (GPTs, Claude, local LLMs, etc.) and let it consume tools like done locally. 
- [Improve LLMs With Proxy-Tuning](https://lightning.ai/lightning-ai/studios/improve-llms-with-proxy-tuning) and avoid weight changes in the model. Sometimes an LLM can be too resource intensive to train, or the user does not have access to the LLM's weights. 


## 🔖 Book'mark

- Book: [Building a Cyber Risk Management Program](https://www.oreilly.com/library/view/building-a-cyber/9781098147785/), by Brian Allen, Brandon Bapst, Terry Allan Hicks, released December 2023. 
- [The complete WebAssembly course](https://www.youtube.com/watch?v=eYekV2Do0YU) by KubeSimplify
- [Awesome AI agents](https://e2b.dev/ai-agents/open-source)
- [Awesome Platform Engineering Tools](https://github.com/seifrajhi/awesome-platform-engineering-tools)


## 🎯 Release speed-run

- [Cilium 1.15](https://github.com/cilium/cilium/releases/tag/v1.15.0) brings many enhancements, with Gateway API 1.0 support for Cilium as Kubernetes ingress, correlating traffic to network policies in Kubernetes, and [much more](https://isovalent.com/blog/post/cilium-1-15/).
- [Kibana 8.12](https://www.elastic.co/guide/en/kibana/8.12/release-notes-8.12.0.html) supports Elastic's a new query language ES|QL. A short video shows [how to edit a visualization within a dashboard](https://twitter.com/xeraa/status/1751013939641545001?s=12).
- [OpenTofu 1.6.1](https://github.com/opentofu/opentofu/releases/tag/v1.6.1) provides bugfixes and performance improvements for the first stable 1.6.0 release after forking from HashiCorp Terraform.
- [containerd 2.0.0-beta2](https://github.com/containerd/containerd/releases/tag/v2.0.0-beta.2) focusses on stability of features introduced in 1.x, and removes deprecated features, too.
- [Quickwit](https://quickwit.io/) support has been [added](https://github.com/falcosecurity/falcosidekick/pull/736) to [Falco](https://falco.org/) log outputs. 
- [OpenLLMetry](https://github.com/traceloop/openllmetry) now supports Google Gemini (source: [LinkedIn](https://www.linkedin.com/posts/traceloop_openllmetry-now-fully-supports-googles-gemini-activity-7151222946869436418-91A8?utm_source=share&utm_medium=member_desktop)). 
- [Go 1.22](https://go.dev/doc/go1.22) provides improvements to for loops: variable iteration safety, and support for ranges over integer values. Go 1.22 also includes the first “v2” package in the standard library, [math/rand/v2](https://pkg.go.dev/math/rand/v2).

## 🎥 Events and CFPs

- Mar 15: [DevOpsDays LA](https://devopsdays.org/events/2024-los-angeles/welcome/) in Pasadena, CA. 
- Mar 18-20: [SRECON Americas](https://www.usenix.org/conference/srecon24americas) in San Francisco, CA. 
- Mar 17-18: [Cloud Native Rejekts](https://cloud-native.rejekts.io/) in Paris, France. 
- Mar 19-22: [KubeCon EU 2024](https://events.linuxfoundation.org/kubecon-cloudnativecon-europe/) in Paris, France. 
- Apr 8-10: [QCon London](https://qconlondon.com/) in London, UK.
- Apr 16-17: [DevOpsDays Zurich](https://devopsdays.org/events/2024-zurich/welcome/) in Winterthur, Switzerland. 
- Apr 16-18: [Open Source Summit NA 2024](https://events.linuxfoundation.org/open-source-summit-north-america/) in Seattle, Washington. 
- May 7: [KubeHuddle Toronto 2024](https://ca.kubehuddle.com/) in Toronto, Canada. 
- May 14-15: [DevOpsDays Seattle](https://devopsdays.org/events/2024-seattle/welcome/) in Seattle, WA.
- Jun 10-12: [Monitorama 2024](https://monitorama.com/2024/pdx.html) in Portland, OR. 
- Jun 17-21: [Cloudland 2024](https://www.cloudland.org/en/home/) in Phantasialand near Cologne, Germany. 
- Jun 19-21: [DevOpsDays Amsterdam](https://devopsdays.org/events/2024-amsterdam/welcome/) in Amsterdam, The Netherlands. 
- Jun 20: [Kubernetes Community Days Italy](https://community.cncf.io/events/details/cncf-kcd-italy-presents-kcd-italy-2024/) in Bologna, Italy. 
- Jul 1-2: [Kubernetes Community Days Munich 2024](https://www.kcdmunich.com/) in Munich, Germany. 
- Sep 3-4: [Container Days 2024](https://www.containerdays.io/containerdays-conference-2024/) in Hamburg, Germany.
- Sep 16-18: [Open Source Summit EU 2024](https://events.linuxfoundation.org/open-source-summit-europe/) in Vienna, Austria. 
- Sep 26-27: [DevOpsDays London](https://devopsdays.org/events/2024-london/welcome/) in London, UK. 
- Nov 12-15: [KubeCon NA 2024](https://events.linuxfoundation.org/kubecon-cloudnativecon-north-america-2024/) in Salt Lake City, Utah. 

👋 CFPs due soon 

- May 7: [KubeHuddle Toronto 2024](https://ca.kubehuddle.com/) in Toronto, Canada. [CFP](https://ca.kubehuddle.com/#call-for-proposals) closes Feb 9. 
- Jul 1-2: [Kubernetes Community Days Munich 2024](https://www.kcdmunich.com/) in Munich, Germany. [CFP](https://sessionize.com/kcd-munich-2024-cfp/) closes Mar 31. 
- Sep 3-4: [Container Days 2024](https://www.containerdays.io/containerdays-conference-2024/) in Hamburg, Germany. [CFP](https://www.containerdays.io/containerdays-conference-2024/call-for-papers/) closes Mar 31. 

_Looking for more CfPs?_ 

- [Developers Conferences Agenda](https://developers.events/) by Aurélie Vache.
- [Kube Events](https://kube.events/call-for-papers).
- [GitLab Speaking Resources handbook](https://about.gitlab.com/handbook/marketing/corporate-communications/speaking-resources/).


## 🎤 Shoutouts 

Thanks for the laugh, Dragoi Ciprian. 

> [Client did not pay?](https://github.com/kleampa/not-paid) Add opacity to the body tag and decrease it every day until their site completely fades away.

🌐 

Thanks for reading! If you are viewing the website archive, make sure to [subscribe](https://buttondown.email/opsindev.news) to stay in the loop! See you next month  🤗 

Cheers,
Michael 

PS: If you want to share items for the next newsletter, just reply to this newsletter, [send a merge request](https://gitlab.com/dnsmichi/opsindev.news/blob/main/docs/next.md), or let me know through [LinkedIn](https://www.linkedin.com/in/dnsmichi/), [Twitter/X](https://twitter.com/dnsmichi), [Mastodon](https://crashloop.social/@dnsmichi), [Blue Sky](https://bsky.app/profile/dnsmichi.bsky.social). Thanks! 

