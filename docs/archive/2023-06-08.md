---
image: https://opsindev.news/assets/images/ebpf_clippy.jpeg
---

# 2023-06-08: GitOps energy efficiency, DevPod development environments, Learning eBPF & generative AI, Bridgekeeper Kubernetes policies, OpenObserve, Grafana Pyroscope, Coroot distributed tracing 

_Thanks for reading the web version, you can subscribe to the [Ops In Dev newsletter](https://buttondown.email/opsindev.news) to receive it in your mail inbox._

<iframe
scrolling="no"
style="width:100%!important;height:220px;border:1px #ccc solid !important"
src="https://buttondown.email/opsindev.news?as_embed=true"
></iframe><br /><br />

<!-- Copy everything below into the Buttondown newsletter form. -->

## 👋 Hey, lovely to see you again

May brought fresh ideas into AI efficiency ideas in DevSecOps workflows, learning progress on eBPF (and many walls I ran into), insights into remote development environments, and great initiatives touching energy efficiency and reducing CO2 emissions. More focus on learning technology and how to adopt it, and so many ideas. Great to see so many tutorials and free learning resources for AI and eBPF. Thanks, everyone! 

There are also great Observability stories with OpenObserve, Grafana Pyroscope, and a cloud-native highlight with Bridgekeeper for Kubernetes policies in Python, next to OPA and Kyverno. There are more release highlights with tracee, trivy, and Coroot adding distributed tracing support with visual heatmaps. 

**Tip**: If you plan to join KubeCon NA in Chicago in November, be quick with CFP submissions - the due date is June 18th. Kubernetes Community Days UK and Australia will also close their CFPs soon. I will be at Cloudland in Phantasialand, Germany on June 19-23, and later in September, at Container Days in Hamburg. 


## 🌱 The Inner Dev learning ... 

### 🐝 The Inner Dev learning eBPF

I wrote about my eBPF learning story in a new InfoQ.com article: ["Learning eBPF for better Observability"](https://go.gitlab.com/f8rXxy). Tackling the getting started experience, which tools to try, what programming languages and demos to look at, to finding use cases, and developing and testing eBPF in CI/CD workflows. My learning story continues, a second article will dive into tools for debugging in production. I'm also working on a learning eBPF workshop and more ideas and insights in future talks (excited for CloudLand and Container Days!).

![Learning eBPF - need help?](https://opsindev.news/assets/images/ebpf_clippy.jpeg)

Chilling in a hammock, surrounded by nature, after resetting my busy brain, I finally have read [Liz Rice's great book "Learning eBPF"](https://www.oreilly.com/library/view/learning-ebpf/9781098135119/) and learned many more insights and ways to understand eBPF. The book follows a great learning curve, and combined with my experience as Ops focussed developer I could dive into many of the C examples to verify and see something. I now understand the challenges with [Compile-Once-Run-Everywhere (CO-RE)](https://o11y.love/topics/ebpf/#co-re-compile-once-run-everywhere) better and how BTF is used to update changes between Kernel versions - just wow! (explaining that here would be too much. Get the book, it is worth the money. You can skip complex topics if you want to dive directly into the concepts with Cilium and Tetragon at the end). I need time to write a thorough book review; for now, know that the "Learning eBPF" book ranks as high as "Hacking Kubernetes" in my recommendations for [fantastic learning resources](https://o11y.love/topics/ebpf/#books-and-blog-posts).

"There is a tooling gap to be filled between something like cosign or notary validating that an OCI image can be trusted and the kernel being able to say that an executable file from that image can be trusted to run eBPF programs." -- [Liz Rice on Twitter](https://twitter.com/lizrice/status/1665774554839502849?s=20). A very interesting idea indeed, [eCHO Episode 93: BPF Signing](https://www.youtube.com/watch?v=8mTWsFUAURE). Watch this space. 

More learning resources have been added to the [eBPF topic on o11y.love](https://o11y.love/topics/ebpf/), here are a few more recommendations:

- [Building an XDP eBPF Program with C and Golang: A Step-by-Step Guide](https://dev.to/pemcconnell/building-an-xdp-ebpf-program-with-c-and-golang-a-step-by-step-guide-4hoa)
- [Distributed patterns compared: Frameworks vs. K8s vs. Service Mesh vs. eBPF](https://www.youtube.com/watch?v=uV_mS2c93Q8) by Matthias Haeussler and Tiffany Jernigan at Devoxx UK 
- [Life Without Sidecars - Is eBPF's Promise Too Good to Be True?](https://www.youtube.com/watch?v=onQuRBy5rgo)
- Google created [buzzer](https://github.com/google/buzzer), a fuzzer toolchain to write eBPF fuzzing strategies. These generate random eBPF programs and then validate that they do not have unexpected behavior in running on a Linux Kernel. 

### 🤖 The Inner Dev learning AI/ML 

If you are wondering what Google Bard does differently to ChatGPT, [this Twitter thread](https://twitter.com/youneedarobot/status/1657776794617929729?s=66) sheds some light: Internet access for real-time information, plugins for everyone, code suggestions, images in prompt results, image search, export data into Google docs or send via Gmail, website/article summary, multiple draft support, voice prompting, SEO companion. 

Google published a [learning path series for generative AI](https://go.gitlab.com/Qdm5mN). The courses include an introduction to generative AI, Large Language Models (LLMs), Responsible AI, Generative AI Fundamentals, Image Generation, Encoder-Decoder Architecture, Attention Mechanism, Transformer Models/BERT Model, Image Captioning Models, Generative AI Studio, and Generative AI Explorer - Vertex AI. Thanks Emilio Salvador for sharing. 

[Elastic announced their Elasticsearch Relevance Engine(tm)](https://www.elastic.co/blog/may-2023-launch-announcement), claiming it will be advanced search for the AI revolution ([LinkedIn post](https://www.linkedin.com/posts/philippkrenn_elasticsearch-activity-7066781376062103552-Q_pR/?utm_source=share&utm_medium=member_ios)). Philipp Krenn also shared talk slides for an deep-dive into [Elasticsearch: Vector and Hybrid Search](https://xeraa.net/talks/elasticsearch-vector-hybrid-search/). Very interesting to see where AI, LLMs and search engines are heading. 

ProjectDiscovery.io released [`aix`, a CLI tool to interact with OpenAI GPT3.5 and GPT4](https://github.com/projectdiscovery/aix) ([LinkedIn post](https://www.linkedin.com/posts/projectdiscovery_install-aix-activity-7067439743621378048-O89x?utm_source=share&utm_medium=member_desktop)).

[slogpt.ai](https://slogpt.ai/) can generate a [Service Level Objective](https://o11y.love/topics/slo/) based on a monitoring graph screenshot. It invites to ask the prompt questions about the suggested SLO, even singing a SLO song. 

You can read more about my AI learning stories soon on the GitLab blog; meanwhile, I recommend following the [AI/ML category](https://about.gitlab.com/blog/categories/ai-ml/) and dive into [code suggestions enhancements](https://go.gitlab.com/OKY2jd), [AI-assisted merge request review summaries](https://go.gitlab.com/wkAWFE) and many more efficiency insights. Cannot wait to apply them to my workflows :-) (this newsletter is written in Markdown using the GitLab Web IDE which will [soon have code suggestions](https://gitlab.com/groups/gitlab-org/-/epics/10549) btw.) 


## 🛡️ The Sec in Ops in Dev 

Loft [introduced DevPod](https://loft.sh/blog/introducing-devpod-codespaces-but-open-source/), which empowers users to control where they run their development environment. It does not host or manage the dev environments; instead, it allows to define the environment which can be run on cloud infrastructure or localhost with Docker or Kubernetes. DevPod describes dev environments using the open [DevContainer standard](https://containers.dev/).

It is great to see that "bring your own infrastructure" for development environments resonates well with users. Last month, I looked into provisioning Kubernetes clusters to integrate with the [cloud-based development environments beta in GitLab](https://go.gitlab.com/QbYeNi) ([DevRel use cases group](https://go.gitlab.com/EmV0x2), blog post soon). Platform engineers can take advantage of securing the workspaces, control where the data is stored, and overall provide developers a better development experience. 

Learning from production incidents is great. Highly recommend subscribing to the Netflix Technology blog for great deep-dives, this month is about: [Debugging a FUSE deadlock in the Linux kernel](https://netflixtechblog.com/debugging-a-fuse-deadlock-in-the-linux-kernel-c75cd7989b6d). 

## 👁️ Observability

Gergely Orosz [shared an interesting observation with regards to SaaS vs self-managed Observability](https://twitter.com/gergelyorosz/status/1656334991620440079?s=66):

> “Our {popular observability vendor} bill is out of control so we decided to move this in-house, using Prometheus+Grafana.”
> I am hearing this a lot, these days. And teams are following through.
> “The UX is not as good, but we expect to save $$” is feedback from teams that did it.

Some responses highlight what I see too: You do not want to push raw Observability data to SaaS vendors but use filters and pipeline ingestion before sending the data somewhere. The OpenTelemetry collector supports [transforming telemetry](https://opentelemetry.io/docs/collector/transforming-telemetry/) and sends the data to local or SaaS services later. Also, a choice has to be made whether the data is useful for the range of an incident or needs long-term storage which can be very expensive. 

[OpenObserve](https://openobserve.ai/) ([LinkedIn post](linkedin.com/posts/openobserve_github-openobserveopenobserve-10x-activity-7072135113601351680-KghN/)) launched this week as a new open source alternative for Elastic/Splunk/Datadog. While its primary focus seems to be log management, more features support more Observability data ingestion (metrics, traces). OpenObserve stores data in a columnar format to support logs, metrics, and traces. The query language is based on SQL. Real-time and scheduled alerts with different targets can also be sent to Prometheus AlertManager. PromQL support for metrics is [in development](https://openobserve.ai/docs/#project-status-features-and-roadmap). OpenTelemetry support comes with [instrumenting code to send traces](https://openobserve.ai/docs/ingestion/traces/opentelemetry/), and [sending metrics via Prometheus write to OpenObserve](https://openobserve.ai/docs/ingestion/metrics/otel_collector/). The [announcement blog post](https://openobserve.ai/blog/launching-openobserve/) shares more insights on the reasons, and [future roadmap](https://openobserve.ai/docs/#project-status-features-and-roadmap). You can try the free tier SaaS, or [install OpenObserve](https://openobserve.ai/docs/quickstart/) in your infrastructure. 

Pyroscope was acquired by Grafana earlier this year, now the first milestone has been announced: [Continuous Profiling is in public preview in Grafana Cloud](https://grafana.com/blog/2023/06/07/continuous-profiling-now-in-public-preview-in-grafana-cloud/). In the background, engineering work was done to merge Grafana's own project Phlare with Pyroscope, and keep features like horizontally scaling. The Grafana UI was enhanced to visualize and analyze profiling data. 

More Observability learning resources this month:

- [PromQL Data Selection Explained | Selectors, Lookback Delta, Offsets, and Absolute "@" Timestamps](https://www.youtube.com/watch?v=xIAEEQwUBXQ)
- [Exploring Kubernetes Observability with OpenTelemetry](https://www.youtube.com/watch?v=50XZxtrvjU8)


## 🌤️ Cloud Native 

[Bridgekeeper](https://github.com/MaibornWolff/bridgekeeper) is a new Kubernetes policy enforcement framework using Python as configuration language. It aims to be easier than Rego (Open Policy Agent). Bridgekeeper defines a custom resource definition called `Policy` which defines the rules with inline Python scripts. An [example policy](https://github.com/MaibornWolff/bridgekeeper/blob/main/example/policy.yaml) shows how to check for container images using the latest tag, by using Python dictionaries and string manipulation methods in 7 lines of code. Recommend to bookmark and try Bridgekeeper! Thanks to [Christian Heckelmann](https://twitter.com/wurstsalat) for sharing. 

The talk [Evaluating the Energy Footprint of GitOps Architectures: A Benchmark Analysis](https://www.youtube.com/watch?v=LEEEivdYns4) compares energy consumption and CO2 emissions of different GitOps architectures with a series of experiments. The research uses [Kepler](https://github.com/sustainable-computing-io/kepler), a light-weight low level power consumption metrics exporter. The research provides insights into the decision making for energy efficiency in the cloud-native ecosystems. 

More cloud native learning resources: 

- [The surprising truth about Cloud Egress Costs](https://medium.com/@alexandre_43174/the-surprising-truth-about-cloud-egress-costs-d1be3f70d001)
- [David Flanagan - What I Learnt Fixing 50+ Broken Kubernetes Clusters - WTF is SRE 2023](https://www.youtube.com/watch?v=A5HbfExtZbU)

## 📚 Tools and tips for your daily use

- [gron](https://github.com/tomnomnom/gron) transforms JSON into discrete assignments to make it easier to grep for what you want and see the absolute 'path' to it.
- [Altair GraphQL client](https://github.com/altair-graphql/altair), a beautiful feature-rich GraphQL Client for all platforms.
- [Crane](https://github.com/google/go-containerregistry/tree/main/cmd/crane#install-via-brew) is a tool for interacting with remote images and registries.
- [How to use Podman in GitLab Runners](https://go.gitlab.com/2kL5Z3). 
- [Debug “credentials from /root/.docker/config.json” vs “credentials from payload” with GitLab Runner and Container Registry](https://go.gitlab.com/JAoCjh). 
- [Lima](https://github.com/lima-vm/lima), Linux virtual machines (on macOS in most cases) provides a great framework to create demo environments. Works on Apple M1 Silicon to run Ubuntu 23 LTS for example.
- [Monokle](https://github.com/kubeshop/monokle) streamlines the process of creating, analyzing, and deploying Kubernetes configurations by providing a unified visual tool for authoring YAML manifests, validating policies, and managing live clusters. 
- Efficiency tip: [Search all open tabs in Chrome: `cmd shift a`](https://support.google.com/chrome/answer/157179?hl=en).

## 🔖 Book'mark

- [Learning eBPF](https://www.oreilly.com/library/view/learning-ebpf/9781098135119/) by Liz Rice, published in March 2023. 
- [Head First Python, 3rd Edition](https://www.oreilly.com/library/view/head-first-python/9781492051282/), will be published in August 2023. I learned Python from scratch with the 2nd edition in ~2017, great learning curve throughout the book. 
- [Google learning path series for generative AI](https://go.gitlab.com/Qdm5mN).
- Verify your container knowledge by these interview questions, [shared by Alex Jones on Twitter](https://twitter.com/alexjonesax/status/1659467621115342848?s=66).


## 🎯 Release speed-run

[Kepler (Kubernetes Efficient Power Level Exporter)](https://github.com/sustainable-computing-io/kepler) released 0.5, and onboarded as CNCF sandbox project. [Prometheus 2.44.0](https://github.com/prometheus/prometheus/releases/tag/v2.44.0) adds health and readiness checks into Promtool CLI, and improvements for remote-write/read. [OpenSearch 2.7.0](https://opensearch.org/blog/get-started-opensearch-2-7-0/) and [2.8.0](https://opensearch.org/blog/opensearch-2.8.0-released/) bring GA for searchable snapshots and segment replication, multiple data sources support for OpenSearch Dashboards, and much more. [Coroot 0.17.0](https://github.com/coroot/coroot/releases/tag/0.17.0) supports distributed tracing through OpenTelemetry. The coroot agent can auto-instrument protocols including HTTP, PostgreSQL, MzSQL, Redis, etc. to collect more tracing insights. The traces are stored in ClickHouse. The UI provides great visualization features, such as heatmaps to make it easier to pinpoint specific traces such as slow requests or errors.  

[tracee v0.15.0](https://github.com/aquasecurity/tracee/discussions/3213) allows to configure tracee using Kubernetes ConfigMaps, adds new policy actions `webhook`, `forward`, introduces experimental event data sources, and supports capturing read operations. [trivy v0.42.0](https://github.com/aquasecurity/trivy/discussions/4541) allows to convert JSON reports into different formats, shows digests for OS packages, adds support for scanning Terraform Plan files. [Kyverno v1.10](https://nirmata.com/2023/05/30/kyverno-release-1-10/) increases scalability with service decomposition, and extensibility via external service call support. It also adds support for CNCF Notary to help software supply chain security. [Chaos Mesh v2.6.0](https://github.com/chaos-mesh/chaos-mesh/releases/tag/v2.6.0) adds more stability, and by default runs the Chaos DNS server for simulating DNS faults. 

[Rust 1.70.0](https://blog.rust-lang.org/2023/06/01/Rust-1.70.0.html) adds stable support for `OnceCell`, `OnceLock`, `IsTerminal` and now enforces stability in the test CLI. [GitLab 16.0](https://go.gitlab.com/vlcg4Q) brings code suggestions and remote development environments in beta, GPU enabled SaaS runners on Linux, comment templates, fork sync on the GitLab UI, error tracking GA, reusable CI/CD components, and a new menu navigation as opt-in (default in 16.1 on June 22). 


## 🎥 Events and CFPs

- Jun 15: [Kubernetes Community Days Zurich](https://kcdzurich.ch/) in Zurich, Switzerland. 
- Jun 20-23: [CloudLand 2023](https://www.cloudland.org/en/home/), at [Phantasialand](https://en.wikipedia.org/wiki/Phantasialand), Germany. See you there, 2 talks accepted!  
- Jun 26-28: [Monitorama 2023](https://monitorama.com/2023/pdx.html) in Portland, OR. 
- Jul 17-18: [Kubernetes Community Days Munich](https://community.cncf.io/events/details/cncf-kcd-munich-presents-kcd-munich-2023/), in Munich, Germany.
- Aug 9-10: [DevOpsDays Chicago 2023](https://devopsdays.org/events/2023-chicago/welcome/) in Chicago, IL.
- Aug 22: [Kubernetes Community Days Australia](https://blog.bradmccoy.io/introducing-kubernetes-community-days-australia-793cb10a0faf) in Sydney, Australia.
- Sep 11-13: [Container Days EU 2023](https://www.containerdays.io/) in Hamburg, Germany. See you there! 
- Oct 2-6: [DEVOXX Belgium](https://devoxx.be/), Antwerp, Belgium.
- Oct 6-7: [DevOps Camp Nuremberg](https://twitter.com/devops_camp/status/1628678706154680321), Nuremberg, Germany. See you there! 
- Oct 10-12: [SRECON EMEA](https://www.usenix.org/conference/srecon23emea) in Dublin, Ireland. 
- Oct 17-18: [Kubernetes Community Days UK](https://community.cncf.io/events/details/cncf-kcd-uk-presents-kubernetes-community-days-uk-2023/) in London, UK. 
- Nov 6-9: [KubeCon NA 2023](https://events.linuxfoundation.org/kubecon-cloudnativecon-north-america/), Chicago, IL. Planning to be there. 
- Nov 16-17: [Continuous Lifecycle / Container Conf](https://www.continuouslifecycle.de/) in Mannheim, Germany. 

👋 CFPs due soon 

- Aug 22: [Kubernetes Community Days Australia](https://blog.bradmccoy.io/introducing-kubernetes-community-days-australia-793cb10a0faf) in Sydney, Australia. [CFP](https://kcd.smapply.io/prog/kcd_australia_cfp_2023/) closes June 30. 
- Oct 17-18: [Kubernetes Community Days UK](https://community.cncf.io/events/details/cncf-kcd-uk-presents-kubernetes-community-days-uk-2023/) in London, UK. [CFP](https://kcd.smapply.io/prog/kcd_uk_2023/) closes July 2. 
- Nov 6-9: [KubeCon NA 2023](https://events.linuxfoundation.org/kubecon-cloudnativecon-north-america/), Chicago, IL. [CFP](https://events.linuxfoundation.org/kubecon-cloudnativecon-north-america/program/cfp/) closes June 18. **next week** 

_Looking for more CfPs?_ 

- [CFP Land](https://www.cfpland.com/).
- [Developers Conferences Agenda](https://github.com/scraly/developers-conferences-agenda) by Aurélie Vache.
- [Kube Events](https://kube.events/call-for-papers).
- [GitLab Speaking Resources handbook](https://about.gitlab.com/handbook/marketing/corporate-communications/speaking-resources/).


## 🎤 Shoutouts 

- "Tell a joke only a programmer would understand 👀" -- [Florin Pop on Twitter](https://twitter.com/florinpop1705/status/1660971077869109249?s=66). 
- Thanks Daniel Finneran for this newsletter's [social image](https://twitter.com/thebsdbox/status/1658759434418495488).



🌐 

Thanks for reading! If you are viewing the website archive, make sure to [subscribe](https://buttondown.email/opsindev.news) to stay in the loop! 

See you next month - let me know what you think on [LinkedIn](https://www.linkedin.com/in/dnsmichi/), [Twitter](https://twitter.com/dnsmichi), [Mastodon](https://crashloop.social/@dnsmichi).

Cheers,

Michael 

PS: If you want to share items for the next newsletter, please check out the [contributing guide](https://opsindev.news/contributing/) - tag me in the comments, send me a DM or [submit this form](https://forms.gle/YNYm5MVWLe463bHN8). Thanks! 
