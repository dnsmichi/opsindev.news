# 2022-09-13: Container Days, eBPF and OpenTelemetry everywhere, Kubernetes Observability, SBOM Operator, Bomber, Podman as GitLab executor, WebAssembly WASI

<!-- Keep this line for the web version. -->
_Thanks for reading the web version, you can subscribe to the [Ops In Dev newsletter](https://buttondown.email/opsindev.news) to receive it in your mail inbox._

<iframe
scrolling="no"
style="width:100%!important;height:220px;border:1px #ccc solid !important"
src="https://buttondown.email/opsindev.news?as_embed=true"
></iframe><br /><br />

## 👋 Hey, lovely to see you again

We've made it to 100+ subscribers. Thanks, everyone, for reading and sharing valuable content and conversations! I was at Container Days in Hamburg, Germany, last week with great talks, ideas, and impressions - read on in the hot topics section this time. The learning curve in this newsletter follows a path - from security to container builds to eBPF as the hottest topic, to more OpenTelemetry and observability-driven workflows - to some tools and tips that I have found very useful, or got a tip from friends. I've tried to capture complex topics such as WebAssembly too. Choose your own pace to read, learn and iterate :-)

## ☕ Hot Topics

### 📦 Container Days Event Recap

[Container Days](https://www.containerdays.io/) felt like a smaller KubeCon event, with many engaged folks in cloud-native, Kubernetes, security, and observability topics, coming together in Northern Germany. Hamburg also provides real big container ships, [thanks, Nico, for the impressions](https://twitter.com/nmeisenzahl/status/1567485074018295808). 

![Real containers in Hamburg, Germany](https://opsindev.news/assets/images/container-hamburg.jpeg)

The event was packed with four parallel tracks to choose from and left room for many conversations in-between. Day 1 ended with a [relaxed after-party](https://twitter.com/nmeisenzahl/status/1566815671111274498), reserving energy for a fully packed day 2. I remember talking a lot, especially with Dotan Horovits after his amazing [Distributed Tracing talk](https://twitter.com/dnsmichi/status/1567064030078009345), losing my voice in [my talk](https://twitter.com/dnsmichi/status/1566898736932044800), watching fascinating demos from [Nico Meisenzahl hijacking Kubernetes](https://twitter.com/dnsmichi/status/1566765567540854784) and [Liz Rice using eBPF for security observability](https://twitter.com/dnsmichi/status/1567089092713660416), and of course meeting new folks and interesting projects :-) 

[Stormforge](https://www.stormforge.io/) follows a similar approach to [kubecost](https://www.kubecost.com/)/[infracost](https://www.opencost.io/), combined with performance testing, and aiming to reduce Kubernetes infrastructure costs. Interesting combination and product; reach out to [Johanna Luetgebrune](https://www.linkedin.com/in/johanna-luetgebrune-46b66864/) to learn more. I've also taken note to try the [Kubermatic Kubernetes Platform, announcing release 2.21](https://www.kubermatic.com/blog/release-kkp-2-21/). New friends from Novatec brought me into the world of [event-driven architecture by Mirna Alaisami](https://twitter.com/dnsmichi/status/1566739599312953346), Azure, Java, Camunda, Kafka, and new ways to look at [Observability in Kubernetes by Matthias Haeussler](https://twitter.com/dnsmichi/status/1567076605851303938) - thanks for the great conversations :)

Post-event, the Hamburg cloud-native meetup organized a session where Nico Meisenzahl and Philip Welz shared how to prevent your Kubernetes cluster from being hacked. [The slides](https://www.slideshare.net/nmeisenzahl/how-to-prevent-your-kubernetes-cluster-from-being-hacked) are a wealth of knowledge and ideas. Philip reproduced the Cilium Tetragon demo from Liz Rice's Container Days talk by defining a policy that triggers when someone malicious edits `/etc/passwd`, which is very impressive.

The Container Days talk recordings will be available on [their YouTube channel](https://www.youtube.com/channel/UCi1CejrHbE6QPz37dG9gMFA) soon, I recommend subscribing. Meanwhile, check out a few folks to [follow on Twitter](https://twitter.com/dnsmichi/status/1568204550095396864). See you next year in Hamburg! 


## 🎯 Release speed-run

[Podman v4.2.0](https://github.com/containers/podman/releases/tag/v4.2.0) brings support for GitLab Runner as Docker executor, [v4.2.1](https://github.com/containers/podman/releases/tag/v4.2.1) is the latest bugfix release. [Prometheus v2.38.0](https://github.com/prometheus/prometheus/releases/tag/v2.38.0) provides support for pretty-formatting PromQL expressions in the UI and API. The first bugfix release for the LTS version is available too: [v2.37.1](https://github.com/prometheus/prometheus/releases/tag/v2.37.1). [Chaos Mesh v2.3.0](https://github.com/chaos-mesh/chaos-mesh/releases/tag/v2.3.0) adds support for BlockChaos filesystem experiments, [v2.3.1](https://github.com/chaos-mesh/chaos-mesh/releases/tag/v2.3.1) is the latest bugfix release. 

[Kubernetes 1.25](https://kubernetes.io/blog/2022/08/23/kubernetes-v1-25-release/) graduates PodSecurity Admission to stable. The release is followed by [k3s v1.25.0](https://github.com/k3s-io/k3s/releases/tag/v1.25.0%2Bk3s1). Make sure to follow the [upgrade notes](https://github.com/kubernetes/kubernetes/blob/master/CHANGELOG/CHANGELOG-1.25.md#urgent-upgrade-notes). [kube-state-metrics v2.6.0](https://github.com/kubernetes/kube-state-metrics/releases/tag/v2.6.0) brings metrics for RBAC resources, and the custom resource state metrics enhanced its features. 

[Trivy v0.31](https://lia.mg/posts/trivy-aws/) supports scanning for AWS security issues. [sigstore cosign v1.11.1](https://github.com/sigstore/cosign/releases/tag/v1.11.1) adds many fixes. [GitLab 15.3](https://about.gitlab.com/releases/2022/08/22/gitlab-15-3-released/) moves the pull-based GitOps feature without impersonation to the free tier. 


## 🛡️ The Sec in Ops in Dev 

[Devs don't want to do ops](https://www.infoworld.com/article/3669477/devs-don-t-want-to-do-ops.html)? Do Devs need to do ops in the era of platforms, automated workflows, and abstracted APIs for deployments? The article quotes, "DevOps is dead," yet a platform helps bring DevOps platforms forward and together again. It will be interesting to see how vendors and users move along in 2023 when themes like Observability-driven development and MLOps reach global audiences. 

[Traditional Packaging is not Suitable for Modern Applications](https://theevilskeleton.gitlab.io/2022/08/29/traditional-packaging-is-not-suitable-for-modern-applications.html) is an interesting read. While I agree on moving on to using containers, tools like flatpak for Linux may not entirely solve the problem, looking beyond operating systems e.g. into securing supply chains with common specs on image signing (sigstore/cosign). That's already one step further, and will IMHO become more dominant in the future, also "outside" of cloud-native environments. Microsoft announced [built-in container support for the .NET SDK](https://devblogs.microsoft.com/dotnet/announcing-builtin-container-support-for-the-dotnet-sdk/) and abstracts settings like `FROM` for base images away into MSBuild properties when needed. You can run `dotnet publish` in CI/CD, log into the registry, and push the created image to later deploy in production. 

You have received a JSON file as SBOM (Software Bill of Materials) from your vendor ... what's next? Scan for security vulnerabilities with [Bomber](https://github.com/devops-kung-fu/bomber), for example. It supports SDPX, CycloneDX, and Syft formats and uses OSV or the Sonatype OSS index as a vulnerability information provider. Another cool project is the [SBOM Operator](https://github.com/ckotzbauer/sbom-operator), providing a catalog of all images of a Kubernetes cluster to multiple targets with Syft. It supports storing the SBOM in Git, Dependency Track, OCI-Registry, and ConfigMaps for further (security) analysis. 

## ⛅ Cloud Native

Why you should not use CPU limits in your Kubernetes cluster with multi-threaded microservices - [great learning curve and read](https://medium.com/directeam/kubernetes-resources-under-the-hood-part-3-6ee7d6015965) on how CPU throttling can affect performance negatively. 

You may have heard about Keptn as a quality gate with SLOs and metrics in CI/CD already. [This quickstart guide](https://kubesimplify.com/keptn-getting-started) sheds light on many features: Keptn as a control plane for services, reusable pipelines, asynchronous event-driven workflows, validation gates, and remediation after deployments. 

Creating and maintaining container images is hard, and with a variety of security scanners, many items are detected. But what if ML could help with detection and automated fixes in merge requests? That's a focus area in GitLab's Incubation Engineering team, resulting in [Dokter](https://gitlab.com/gitlab-org/incubation-engineering/ai-assist/dokter/). 
 

## 👁️ Observability

Clearly, [eBPF is a hot topic, and RedMonk follows closely](https://redmonk.com/rstephens/2022/09/08/ebpf/). Liz Rice provided a deep dive into eBPF for better security observability at Container Days, and the possibility to [define Cilium Tetragon tracing policies to follow/prevent syscalls](https://speakerdeck.com/lizrice/ebpf-for-security-observability?slide=35) is fascinating at best. Want to know what is possible with eBPF? [Anomaly detection using unsupervised-learning encodes and eBPF](https://www.evilsocket.net/2022/08/15/Process-behaviour-anomaly-detection-using-eBPF-and-unsupervised-learning-Autoencoders/) explains the principles and follows with monitoring the Spotify process to train the ML model. "Connecting to Facebook" as action later leads to an anomaly detected as a syscall. Wow.
Another great example is [monitoring gRPC-C with eBPF](https://blog.px.dev/grpc-c-tracing/), shining into the problem of gRPC connections applying stateful compression, making observability harder. The article describes challenges with traces, and how the solution can trace data, headers and stream close. Last but not least, I want to close the loop towards Kubernetes Observability and eBPF by pointing you to [a great read](https://medium.com/@isalapiyarisi/getting-started-on-kubernetes-observability-with-ebpf-88139eb13fb2) that combines eBPF probes with Prometheus metrics conversion, enriched with metadata from the Kubernetes API. That's a great way to learn the basics without tool overhead, with the next article on AIOps and anomaly detection coming soon. 

[OpenTelemetry roadmap and latest updates](https://horovits.medium.com/opentelemetry-roadmap-and-latest-updates-a389144f3812) was written in June, but it is never too late to catch up after Dotan provides a talk ;-) The latest OpenTelemetry updates include [adding Profiling as a new type](https://github.com/open-telemetry/oteps/pull/212/files), which was discussed at KubeCon EU in May. Please help review the PR to refine the specification. 

Inspired by reading about [how to instrument Nginx with OpenTelemetry](https://opentelemetry.io/blog/2022/instrument-nginx/), I've started playing with an OpenTelemetry and Jaeger deployment in Kubernetes, and created a dedicated [nginx-opentelemetry project](https://gitlab.com/everyonecancontribute/observability/nginx-opentelemetry). It includes a pre-defined configuration and a container build for Kubernetes deployments to [show-case the demo with chaos engineering](https://docs.google.com/presentation/d/1DDyXj6Lr8DzVxcjHEl3OK7jdEke6I5BEoWuDOBXYGv8/edit#slide=id.g148b160c6c3_0_133) for my Container days talk. 

Log management and analytics are challenging when it comes to scaling and data retention (which logs in my Kubernetes cluster do I need - [great read](https://blog.flant.com/logs-in-kubernetes-expectations-vs-reality/)). [Cloudflare shared the technical implementation of their logging pipeline](https://blog.cloudflare.com/log-analytics-using-clickhouse/), Elasticsearch bottlenecks, and how they adopted Clickhouse as a columnar database with inserts, batch sizes, data modeling, partitioning, and storing different resolutions/sample intervals (ABRs). 


## 🔍 The inner Dev

[Speed up your monorepo workflow in Git](https://about.gitlab.com/blog/2022/09/06/speed-up-your-monorepo-workflow-in-git/) is a great read that provides nifty tricks with sparse checkouts, partial clones, and shallow clones. If you are developing API workflows with Postman, the new [integration with GitLab](https://about.gitlab.com/blog/2022/08/24/postman-integration-with-gitlab-makes-your-api-workflows-easier/) enables developers to think about API elements as the API itself, instead of treating code, API definitions, documentation, collections, tests, monitors, etc. as independent entities. All of these constitute the API.

There is [some controversy going on with WebAssembly and the WASI interface](https://news.ycombinator.com/item?id=32562230), making AssemblyScript remote from its WASI support. [WASI](https://wasi.dev/) provides a modular system interface required by WebAssembly projects to interact with operating system calls, for example. It seems that portability between architectures is not hard, with more concerns shared in [this issue](https://github.com/WebAssembly/WASI/issues/401#issue-817748895). We may see sub-APIs for WebAssembly solving special purpose requests such as time, filesystems, sockets, etc. - until then, developer experience and getting started guides need improvements. (it's even hard to explain in this newsletter - let me know if you want to chat more and learn together)

[Erica Brescia asked a thoughtful question](https://twitter.com/ericabrescia/status/1563595798696189952): "Hey OSS friends - weekend q for you: Can you think of a very widely adopted/successful OSS project that was adopted almost exclusively by non-tech companies (where high tech companies eschewed it)?" Looking into the responses, we came across Shibboleth and Moodle used at academic institutions and OBS Studio for streaming. What's your take? Reply directly on Twitter :-) 

## 📈 Your next project could be ...

- [Why your website should be under 14KB in size](https://endtimes.dev/why-your-website-should-be-under-14kb-in-size/)
- [LEGO Lighthouse 21335](https://www.lego.com/en-us/product/motorised-lighthouse-21335) (but the price tag of 300$/€ scares me a bit, decide yourself)

# 📚 Tools and tips for your daily use

- [glances](https://github.com/nicolargo/glances), a top/htop alternative for GNU/Linux, BSD, Mac OS, and Windows operating systems.
- [devbox](https://github.com/jetpack-io/devbox) is a command-line tool that lets you easily create isolated shells and containers. You start by defining the list of packages required by your development environment, and devbox uses that definition to create an isolated environment just for your application.
- [Debug containers with a temporary pod](https://nikgrozev.com/2020/07/29/debug-container-in-kubernetes/), for example, curl to test HTTP connectivity issues in your Kubernetes cluster. 
- [k8spacket](https://github.com/k8spacket/k8spacket) helps to understand TCP packets traffic in your Kubernetes cluster
- [k9s](https://k9scli.io/), a terminal-based UI to interact with your Kubernetes clusters. 
- [C++ tip](https://twitter.com/lefticus/status/1565729819077160960): Use `\n` for new lines in iostreams, and only flush when desired. `std::endl` always flushes, which can affect performance. 
- [Config grep tips: Remove newline and hashes](https://twitter.com/xeraa/status/1563941205871820805): `grep -v -e '^$' -e'^ *#' $argv` or `egrep -v '^#|^$' $argv` as shell aliases. 
- [Automatically Convert Grafana Dashboards from InfluxQL to PromQL](https://logz.io/blog/influxql-to-promql-grafana-dashboard-converter-open-source/). Based on the work from Aiven.io, Logz.io released [this new OSS tool](https://github.com/logzio/influxql-to-promql-converter) to help migrations. 

## 🔖 Book'mark

- [Get developers up to speed with eBPF Rust development](https://aya-rs.dev/book/), i.e. How to set up a development environment and Share current best practices about using Rust for eBPF 
- Getting started: [How to expose a port for your deployed application in Kubernetes](https://home.robusta.dev/blog/kubernetes-service-vs-loadbalancer-vs-ingress/): Services, ClusterIP, NodePort, LoadBalanacers, Ingress, ... - great learning curve. 
- Alois Reitbauer started a new initiative: The [open source canvas](https://opensource-canvas.org/) is a simple framework based on the business model canvas to model and define open source projects. The canvas focuses on open source projects that co-exist and support commercial software offerings - like open core projects.
- New PromLabs training: [Understanding and Building Prometheus Exporters](https://training.promlabs.com/training/understanding-and-building-exporters)

## 🎥 Events and CFPs

- Sep 22 11am CEST: [OpenObservability Talks: Where Are My App’s Traces?? Instrumentation in Practice](https://www.youtube.com/watch?v=VFykWV1mLAI) with Eden Federman, Co-Founder & CTO at keyval and Dotan Horovits, Logz.io 
- Sep 28-29: [eBPF Summit](https://ebpf.io/summit-2022/), virtual 
- Oct 12-13: [Kubernetes Community Days Munich](https://community.cncf.io/events/details/cncf-kcd-munich-presents-kubernetes-community-days-munich-2022-1/) in Munich, Germany. Join my talk! 
- Oct 24+25: [KubeCon NA co-located events](https://events.linuxfoundation.org/kubecon-cloudnativecon-north-america/program/colocated-events/#co-located-event-schedule). I'll be at [Open Observability day](https://events.linuxfoundation.org/open-observability-day-north-america/), and [SecurityCon](https://events.linuxfoundation.org/cloud-native-securitycon-north-america/program/schedule/)/[GitOpsCon](https://events.linuxfoundation.org/gitopscon-north-america/program/schedule/) cheering to my team mate Dov Hershkovitch talking about JWT security in CI/CD. 
- Oct 26-28: [KubeCon NA](https://events.linuxfoundation.org/kubecon-cloudnativecon-north-america/) in Detroit, Michigan. Join me there at the GitLab booth! 
- Nov 08-09: [PromCon EU](https://promcon.io/2022-munich/) in Munich, Germany 
- Nov 10-11: [All Day DevOps](https://www.alldaydevops.com/), virtual, Join my talk!
- Nov 16-17: [Continuous Lifecycle / Container Conf](https://www.continuouslifecycle.de/) in Mannheim, Germany. Join my talk! 
- Nov 28 - Dec 2: [AWS re:Invent](https://reinvent.awsevents.com/). Join me there at the GitLab booth!

👋 CFPs due soon 

- Apr 17-21: [KubeCon EU 2023](https://events.linuxfoundation.org/kubecon-cloudnativecon-europe/) in Amsterdam. CFP closes Nov 18. 


_Looking for more CfPs? Try [CFP Land](https://www.cfpland.com/)._



## 🎤 Shoutouts 

Kelsey Hightower and Werner Vogels for [sharing the analogy](https://twitter.com/werner/status/1566736285913387008) between open source conferences such as KubeCon, and vendor-specific like VMware Explore. [Full interview](https://www.youtube.com/watch?v=3j9PTlMKis8). 


🌐

Thanks for reading! If you are viewing the website archive, make sure to [subscribe](https://opsindev.news/#how-to-get-the-newsletter) to stay in the loop! 

See you next month - let me know what you think on [Twitter](https://twitter.com/dnsmichi) or [LinkedIn](https://www.linkedin.com/in/dnsmichi/).

Cheers,

Michael 

PS: If you want to share items for the next newsletter, please check out the [contributing guide](https://opsindev.news/contributing/) - tag me in tweet replies or send me a DM. Thanks! 
