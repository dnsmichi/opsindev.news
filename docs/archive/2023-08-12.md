---
image: https://opsindev.news/assets/images/isovalent_cloud_network_map_ebpf.png
---

# 2023-08-12: Learning Rust with AI, eBPF learning map, We Hack Purple+Semgrep, 90DaysOfDevOps, eBPF Summit 2023, Coroot AI Observability, Trace-based testing with OpenTelemetry

_Thanks for reading the web version, you can subscribe to the [Ops In Dev newsletter](https://buttondown.email/opsindev.news) to receive it in your mail inbox._

<iframe
scrolling="no"
style="width:100%!important;height:220px;border:1px #ccc solid !important"
src="https://buttondown.email/opsindev.news?as_embed=true"
></iframe><br /><br />

<!-- Copy everything below into the Buttondown newsletter form. -->

## 👋 Hey, lovely to see you again

The AI landscape is moving fast, and so is Observability and eBPF. The latter will have a virtual eBPF Summit in September 2023 for everyone to join, inspiring new tools that help debug production workloads. Learning AI on the other hand also means finding use cases, for example learning a new programming language and writing a blog post about the experience. I also loved reading about AI Observability and Shadow AI this month. Aside from continued OSS license changes and forks, a happy community moment happened with We Hack Purple joining forces with Semgrep. 

## 🌱 The Inner Dev learning ... 

Recommended read: [Developer-Led Landscape: Software Supply Chain Security](https://tylerjewell.substack.com/p/developer-led-landscape-software) by Tyler Jewell. 

### 🐝 The Inner Dev learning eBPF

[eBPF Summit 2023](https://ebpf.io/summit-2023/) happens on September 13, 2023. I was kindly invited to the CFP review and found many great ideas through the submissions. You know some of them already in this newsletter, others tackle topics in the ever-growing AI and DevSecOps landscape. Stay tuned, and register for the event.

Isovalent created an [interactive learning map for eBPF](https://labs-map.isovalent.com/?journey=cloud-network), and different user persona journeys: Cloud network engineer, Platform engineer, Platform Ops, Cloud Architect, and Security Professionals. Fantastic learning resource. Bookmark this! Thanks [Raphaël Pinson](https://www.linkedin.com/posts/raphink_calling-network-engineers-did-you-know-activity-7094244564873437184-CnwZ?utm_source=share&utm_medium=member_desktop) for sharing. 

![Isovalent cloud network map for eBPF](https://opsindev.news/assets/images/isovalent_cloud_network_map_ebpf.png)

[Coroot Agent 1.9.0](https://github.com/coroot/coroot-node-agent/releases/tag/v1.9.0) supports eBPF-based TLS connection tracing for Golang applications. That way the Coroot agent can capture requests before they are encrypted. The [implementation PR](https://github.com/coroot/coroot-node-agent/pull/27/files) provides insights and can be an inspiration for own learning exercises.

To get better insights into running eBPF programs, [ebpfmon](https://github.com/redcanaryco/ebpfmon) can help. It builds on top of bpftool and provides a terminal UI. 


### 🤖 The Inner Dev learning AI/ML 

This month, I focussed on sharing my learning experience for a new programming language and how AI can help. It turned out to be a fun challenge to "instrument" GitLab Duo Code Suggestions in a way that I got better suggestions. Learn more in the blog post [Learning Rust with a little help from AI](https://go.gitlab.com/Bs6ls8). Stay tuned for the second part on Rust with more playful advanced learning and more programming languages. 

[Gergely Orosz](https://www.linkedin.com/in/gergelyorosz/) shared [how to block OpenAI and ChatGPT from scraping](https://www.linkedin.com/posts/gergelyorosz_i-updated-my-blogs-robotstxt-to-opt-out-activity-7094762821527171072-8DYn?utm_source=share&utm_medium=member_desktop) their blog and newsletter. The reasons are insightful: Training the models gives no attribution to original content authors. The OpenAI LLMs are not publicly available and do not benefit the community. The discussion on LinkedIn led me to a great [resource collection to learn LLMs](https://telling-light-53d.notion.site/LLMs-Tutorials-641cb3773b0048d0b142e8bdb70bc94b) and more by [Nadia Privalikhina](https://www.linkedin.com/in/nadezhda-privalikhina/). 

Elastic wrote about [Avoid Shadow AI — Embrace generative AI in the SOC](https://www.elastic.co/blog/avoid-shadow-ai-generative-ai-soc), raising awareness for the question: Should we allow the use of generative AI within our organizations, even within cybersecurity? And if yes, how to maintain control of the data to avoid leaking sensitive information into a public LLM. For the Elastic AI Assistant, they embraced full transparency showing all data exchanged and added functions to anonymize data. The article concludes with reminding to embrace and empower teams, to avoid the creation of Shadow AI (after Shadow IT). 

The talk [AI Observability at Meta Scale](https://youtu.be/9_iLFbNWoE8) dives into how Meta observes and optimizes resource usage for AI workloads. Four different layers of observability are discussed: Fleet level resource usage (Aggregation and regression tracking), Meta performance (profiling/analysis platform), application tracing and instrumentation (Pytorch profiler / Kineto, BPF tracing) and bare metal telemetry and monitoring (Dynolog). 

## 👁️ Observability

To diagnose network issues in Kubernetes, I found [KubeSkoop](https://github.com/alibaba/kubeskoop) from Alibaba which uses eBPF for network analysis. It can draw a network communication map, export metrics to Prometheus, and sends network anomaly events to Grafana Loki. 

[bpftune](https://github.com/oracle/bpftune) from Oracle helps to tune Linux systems automatically by observing their behavior. Supported tuners are congestion, neighbor table, route table, sysctl, TCP buffer, net buffer, netns. The architecture allows loading tuners as plugins and is described as a lightweight daemon without polling too many events.

If you ever wanted to try trace-based testing and did not know how, there is a new guide: [Trace-based Testing the OpenTelemetry demo](https://opentelemetry.io/blog/2023/testing-otel-demo/). The integration tests are AVA tests, while the frontend end-to-end tests use Cypress. The demo environment uses a shop to order products and does the checkout procedure for a better learning curve. 

Coroot shared their [research on adding AI-powered root cause analysis](https://coroot.com/blog/explaining-application-performance-anomalies-with-ai) for their observability platform. Interesting read from finding the right telemetry data, adding system topology, and concluding with an interesting statement: 

> We want AI to assist us with anomaly analysis rather than anomaly detection 


## 🛡️ DevSecOps 

[Daniel Bodky](https://www.linkedin.com/in/daniel-bodky/) shared a great read about [The Good, the Better, and the Ugly - Signing Git Commits](https://dbodky.me/the-good-the-better-and-the-ugly-signing-git-commits/). The good is called `gitsign`, the best is `SSH Commit signing`, and the ugly is -- you guessed it - GPG. If you have tried convincing the GPG agent to sign Git commits with TTY, you probably know the pain. I switched to [SSH key commit signing](https://docs.gitlab.com/ee/user/project/repository/ssh_signed_commits/) some months ago, see the configuration in dotfiles repository: [.gitconfig](https://gitlab.com/dnsmichi/dotfiles/-/blob/main/.gitconfig) and [allowed_signers](https://gitlab.com/dnsmichi/dotfiles/-/blob/main/.ssh/allowed_signers)

The We Hack Purple community, founded by Tanya Janca, and Semgrep are [joining forces](https://semgrep.dev/blog/2023/we-hack-purple-and-i-are-joining-semgrep). This is exciting for many reasons: More security education for everyone, community building at Semgrep, and better developer experience with SAST scanning. Semgrep is already great - I recently looked into the Rust support in Beta. Tip: Order it now if you have not read Tanya Janca's book [Alice and Bob Learn Application Security](https://shehackspurple.ca/books/). Fantastic read. 

The Hurl maintainers revamped their [tutorial pages with a playful web demo](https://hurl.dev/docs/tutorial/your-first-hurl-file.html) which makes it easier to get started with the first command line tests. Hurl allows you to test websites for specific requests and responses and can also be helpful in CI/CD test automation. I wrote a blog post in late 2022 about [Hurl and GitLab CI/CD](https://go.gitlab.com/4QeLMr).


## 🌤️ Cloud Native

Recommended watch: [GitOpsConf 2023: GitLab + Flux!](https://go.gitlab.com/XpMcPl) 

After the RHEL changes to upstream source code access (more in the [July newsletter issue](https://opsindev.news/archive/2023-07-11/#devsecops)), SuSE announced a fork of RHEL which now turned into a collaboration with Oracle and CIQ under the [Open Enterprise Linux Association (OpenELA)](https://www.suse.com/news/OpenELA-for-a-Collaborative-and-Open-Future/). CIQ is the company creating RockyLinux, Oracle maintains Oracle Linux, and SuSE provides RHEL support already (more context [on Hacker News](https://news.ycombinator.com/item?id=36571951)), making this a natural next step to foster open source collaboration. 

More license changes: [HashiCorp changed the license](https://www.hashicorp.com/blog/hashicorp-adopts-business-source-license) of their products to the source-available Business Software License (BuSL), which forbids commercial use in competitive products. Affected are Terraform, Packer, Vault, Boundary, Consul, Nomad, Waypoint, and Vagrant while HashiCorp APIs, SDKs, and almost all other libraries are noted to remain MPL 2.0. The full implications remain unclear, and many companies and users are consulting with lawyers now. Community members started [OpenTerraform](https://github.com/diggerhq/open-terraform), a Terraform fork based on the last commit with the Mozilla Public License 2.0 (MPL). 

Another fork happened in container land: [Incus](https://linuxcontainers.org/incus/) is a fork of LXD, and now part of the Linux Containers project. This happened after Canonical's decision to remove LXD from the Linux Containers project. More insights in the [Hacker News discussion](https://news.ycombinator.com/item?id=36985384).


## 📚 Tools and tips for your daily use

- [jless](https://jless.io/) is a command-line JSON viewer designed for reading, exploring, and searching through JSON data. 
- [Use GitLab and MITRE ATT&CK Navigator to visualize adversary techniques](https://go.gitlab.com/GDbDPI)
- [Kor](https://github.com/yonahd/kor) is a tool to discover unused Kubernetes resources. It can identify ConfigMaps, Secrets, Services, ServiceAccounts, Deployments, StatefulSets, Roles. 
- [Slim Toolkit](https://slimtoolkit.org/) to inspect, optimize and debug your containers
- [Kamaji](https://kamaji.clastix.io/) turns any Kubernetes cluster into an “admin cluster” to orchestrate other Kubernetes clusters called “tenant clusters”. Kamaji is special because the Control Plane components are running in a single pod instead of dedicated machines. This solution makes running multiple Control Planes cheaper and easier to deploy and operate.
- For better and more strict error handling in shell scripts, set `set -Eeuo pipefail`. More tips in [Ten Things I Wish I’d Known About bash](https://zwischenzugs.com/2018/01/06/ten-things-i-wish-id-known-about-bash/).
- [Set up your infrastructure for on-demand, cloud-based development environments in GitLab](https://go.gitlab.com/CivNbw)
- [Optimizing dependency management with Renovate for GitLab with 500+ repositories](https://medium.com/notive/optimizing-renovate-for-gitlab-with-500-repositories-6d225f24ff79)
- [Terramate](https://github.com/terramate-io/terramate) helps implement and maintain highly scalable Terraform projects by adding powerful capabilities such as code generation, stacks, orchestration, change detection, data sharing and more.

## 🔖 Book'mark

- [Top 15 Kubectl plugins for security engineers](https://sysdig.com/blog/top-15-kubectl-plugins-for-security-engineers/) by [Sysdig](https://www.linkedin.com/company/sysdig/).
- [90DaysOfDevOps](https://github.com/MichaelCade/90DaysOfDevOps) by [Michael Cade](https://www.linkedin.com/in/michaelcade1/)
- [Alice and Bob Learn Application Security](https://shehackspurple.ca/books/) by [Tanya Janca](https://www.linkedin.com/in/tanya-janca/)

## 🎯 Release speed-run

[Cilium 1.14.0](https://github.com/cilium/cilium/releases/tag/v1.14.0) brings Effortless Mutual Authentication, Service Mesh, Networking Beyond Kubernetes, High-Scale Multi-Cluster, and more ([announcement blog post](https://isovalent.com/blog/post/cilium-release-114/)). [Tracee v0.17.0](https://github.com/aquasecurity/tracee/discussions/3353) brings a new policy format compatible with Kubernetes CRDs, new flags for enhanced event filtering and simplified event sets. [OpenSearch 2.9.0](https://opensearch.org/blog/introducing-opensearch-2.9.0/) brings search pipelines GA, production-ready neural search, ML framework GA, and monitors and detectors in OpenSearch Dashboards. 

 [jq 1.7rc1](https://github.com/jqlang/jq/releases/tag/jq-1.7rc1) brings the first release after 5 years. [GitLab 16.2](https://go.gitlab.com/ZFOm8R) comes with a new Rich Text editor, improving the editing experience in issues, comments, wiki. It also supports keyless signing with Cosign, a new command palette for efficiency, triggering a Flux synchronization without any configuration and more.

[Prometheus v2.46.0](https://github.com/prometheus/prometheus/releases/tag/v2.46.0), [OpenTelemetry Collector v0.82.0](https://github.com/open-telemetry/opentelemetry-collector/releases/tag/v0.82.0) and [OpenTelemetry Collector Contribut v0.82.0](https://github.com/open-telemetry/opentelemetry-collector-contrib/releases/tag/v0.82.0), [Perses 0.39.0](https://github.com/perses/perses/releases/tag/v0.39.0), [Keptn v1.4.1](https://github.com/keptn/keptn/releases/tag/1.4.1), [Parca agent v0.23.3](https://github.com/parca-dev/parca-agent/releases/tag/v0.23.3), [Flux v2.0.1](https://github.com/fluxcd/flux2/releases/tag/v2.0.1), [Kyverno v1.10.2](https://github.com/kyverno/kyverno/releases/tag/v1.10.2), [Open Policy Agent v0.55.0](https://github.com/open-policy-agent/opa/releases/tag/v0.55.0)


## 🎥 Events and CFPs

- Aug 22: [Kubernetes Community Days Australia](https://blog.bradmccoy.io/introducing-kubernetes-community-days-australia-793cb10a0faf) in Sydney, Australia.
- Sep 11-13: [Container Days EU 2023](https://www.containerdays.io/) in Hamburg, Germany. See you there! 
- Sep 13: [eBPF Summit 2023](https://ebpf.io/summit-2023/), online. 
- Sep 20-21: [Swiss Cloud Native Day](https://cloudnativeday.ch/), Bern, Switzerland. 
- Sep 26-27: [Kubernetes Community Days Austria](https://community.cncf.io/events/details/cncf-kcd-austria-presents-kcd-austria-2023-1/) in Vienna, Austria. 
- Sep 28-29: [PromCon EU 2023](https://promcon.io/2023-berlin/) in Berlin, Germany. 
- Oct 2-6: [DEVOXX Belgium](https://devoxx.be/), Antwerp, Belgium.
- Oct 6-7: [DevOps Camp Nuremberg](https://twitter.com/devops_camp/status/1628678706154680321), Nuremberg, Germany. See you there! 
- Oct 10-12: [SRECON EMEA](https://www.usenix.org/conference/srecon23emea) in Dublin, Ireland. 
- Oct 17-18: [Kubernetes Community Days UK](https://community.cncf.io/events/details/cncf-kcd-uk-presents-kubernetes-community-days-uk-2023/) in London, UK. 
- Nov 6-9: [KubeCon NA 2023](https://events.linuxfoundation.org/kubecon-cloudnativecon-north-america/), Chicago, IL. Planning to be there. 
- Nov 6: [Observability Day at KubeCon NA 2023](https://events.linuxfoundation.org/kubecon-cloudnativecon-north-america/co-located-events/observability-day/), Chicago, IL. 
- Nov 6: [CiliumCon at KubeCon NA 2023](https://events.linuxfoundation.org/kubecon-cloudnativecon-north-america/co-located-events/ciliumcon/), Chicago, IL. 
- Nov 6: [AppDeveloperCon at KubeCon NA 2023](https://events.linuxfoundation.org/kubecon-cloudnativecon-north-america/co-located-events/appdevelopercon/), Chicago, IL. 
- Nov 16-17: [Continuous Lifecycle / Container Conf](https://www.continuouslifecycle.de/) in Mannheim, Germany. 

2024

- Jan 1 - Mar 31: [90DaysOfDevOps 2024 Community Event](https://sessionize.com/90daysofdevops-2024-community-edition/), virtual, online. 

👋 CFPs due soon 

- Sep 28-29: [PromCon EU 2023](https://promcon.io/2023-berlin/) in Berlin, Germany. [CFP](https://promcon.io/2023-berlin/submit/) is due on Aug 18. 

2024

- Jan 1 - Mar 31: [90DaysOfDevOps 2024 Community Event](https://sessionize.com/90daysofdevops-2024-community-edition/), virtual, online. [CFP](https://sessionize.com/90daysofdevops-2024-community-edition/) is due on Nov 2.

_Looking for more CfPs?_ 

- [CFP Land](https://www.cfpland.com/).
- [Developers Conferences Agenda](https://github.com/scraly/developers-conferences-agenda) by Aurélie Vache.
- [Kube Events](https://kube.events/call-for-papers).
- [GitLab Speaking Resources handbook](https://about.gitlab.com/handbook/marketing/corporate-communications/speaking-resources/).


## 🎤 Shoutouts 

"I'm pretty surethe application is somewhere around here" is a great comic meme for Kubernetes. Bill Mulligan went one step further, saying "Fixed it ..." ([more on Twitter/X](https://twitter.com/breakawaybilly/status/1686704567663116288)). 

🌐 

Thanks for reading! If you are viewing the website archive, make sure to [subscribe](https://buttondown.email/opsindev.news) to stay in the loop! 

See you next month - let me know what you think on [LinkedIn](https://www.linkedin.com/in/dnsmichi/), [Twitter/X](https://twitter.com/dnsmichi), [Mastodon](https://crashloop.social/@dnsmichi), [Blue Sky](https://bsky.app/profile/dnsmichi.bsky.social) 🤗 

Cheers,

Michael 

PS: If you want to share items for the next newsletter, please check out the [contributing guide](https://opsindev.news/contributing/) - tag me in the comments, send me a DM or [submit this form](https://forms.gle/YNYm5MVWLe463bHN8). Thanks! 
