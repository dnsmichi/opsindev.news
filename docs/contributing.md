# Contributing

Thanks so much for considering to contribute to the Ops in Dev newsletter! :) 

## Share a URL and item

There are multiple ways to suggest news, articles, blog posts, ideas, etc. for the newsletter.

- [Submit this form](https://forms.gle/YNYm5MVWLe463bHN8) 
- [Create an issue](https://gitlab.com/dnsmichi/opsindev.news/-/issues/new) with suggestions
- [Twitter](https://twitter.com/dnsmichi) (my DMs are open) or [LinkedIn](https://www.linkedin.com/in/dnsmichi/)
- In the [#EveryoneCanContribute cafe Discord](https://everyonecancontribute.com/page/handbook/#discord)

You can also inspect the source code in the [https://gitlab.com/dnsmichi/opsindev.news](https://gitlab.com/dnsmichi/opsindev.news) project and fork it to create a merge request suggesting your changes for the next newsletter. Please follow the instructions below. 

## Submit a merge request with your suggestion

Fork the project and start editing the `docs/next.md` file which contains the draft for the next newsletter issue. It is a living draft, and will be rotated on the next publishing date. 

_Note: If the file is empty, or misses a heading for structure, feel free to tag `@dnsmichi` and propose the change in the merge request too._

## Acceptance Policy

Please note that the newsletter should be a free learning resource, and not a product / marketing pitch. 

There is no guarantee that MRs, issues and suggestions are going to be included in the next newsletter. If you have concerns, please tag `@dnsmichi` in the comments, or reach out in private. 
 
## Development

The newsletter is built with 2 platforms.

- [https://buttondown.email/opsindev.news/](https://buttondown.email/opsindev.news/) uses Buttondown Professional, with API access. Note: Disabled after 2024-03. 
- [https://gitlab.com/dnsmichi/opsindev.news](https://gitlab.com/dnsmichi/opsindev.news) uses GitLab Pages and MkDocs to create a searchable (standaline archive). Note: Archived after 2024-03.

The benefit is that one can develop the newsletter content with live previews in Gitpod, and publish the newsletter using the buttondown API later automatically with CI/CD. Following the thought, if you want to contribute, you can fork the repository and open it in Gitpod or a local environment with MkDocs to write the updates, and send the MR directly. MR reviews, approvals and merges happen iteratively. When the publishing time arrives, the content from `docs/next.md` is copied into the `docs/YYYY-MM-DD.md` file. 

_Shoutout and kudos to [Michael Hausenblas](https://twitter.com/mhausenblas) who created the idea with [o11y.news](https://o11y.news) and was so kind to share MkDocs and buttondown scripts and configuration to learn from!_

### Preview in MkDocs

This project uses MkDocs with the Material theme. In order to render a preview, use the [following options](https://squidfunk.github.io/mkdocs-material/creating-your-site/#previewing-as-you-write).

If you are using a tablet or mobile, we recommend to use [Gitpod](#gitpod) in your browser.

#### Gitpod

Open the project or your fork and change the selector from `Web IDE` to `Gitpod`.

Alternatively, use [https://gitpod.io/#https://gitlab.com/dnsmichi/opsindev.news](https://gitpod.io/#https://gitlab.com/dnsmichi/opsindev.news)

#### Docker

Install Docker and run the container with port-forwarding to localhost:8000. The source directory is mapped into the container.

```
$ docker run --rm -it -p 8000:8000 -v ${PWD}:/docs squidfunk/mkdocs-material`
```

#### Local source environment

Install Python 3.9+ and pip, for example with Homebrew on macOS.

```
$ brew install python
```

opsindev.news requires MkDocs themes and plugins which are defined in `requirements.txt`. Install them with the following command:

```
$ pip install -r requirements.txt
```

Depending on your environment, the `pip` command is called `pip3`.

You can run MkDocs with the following command:

```
$ mkdocs serve
```

More verbose output for debugging:

```
$ mkdocs serve -v
```


#### Static Build

```shell
$ docker run --rm -it -p 8000:8000 -v ${PWD}:/docs squidfunk/mkdocs-material build
```

### Project layout

    mkdocs.yml    # The configuration file.
    docs/
        index.md  # The documentation homepage.
        ...       # Other markdown pages, images and other files.

## Newsletter Publishing Process 

### Writing

- Create/Update `docs/next.md` with the template from `docs/template.md`
    - Optional: Add content to `docs/next.md`
- Create a MR for the next newsletter issue
- Merge the MR to allow contributions
- Add content in follow-up MRs to `docs/next.md` 

### Social Preview

Embed an image, e.g. from a YouTube video. This will be taken for social preview rendering. 

```
[![GitLab Agent for Kubernetes Go Profiling](http://img.youtube.com/vi/XCqiMeeoaYQ/0.jpg)](https://www.youtube.com/watch?v=XCqiMeeoaYQ)
```

### Publishing 

TODO: Create a script for publishing. 

- Move content from `docs/next.md` to `docs/archive/YYYY-MM-DD.md`
- Commit the changes from CI/CD to the GitLab repository

#### Newsletter

- Schedule the newsletter in Buttondown using the API (TODO), or manual copy paste into 

#### Social

TODO: Automate Twitter/LinkedIn posts. Maybe use Buffer's API to enqueue. 


## Platform Details

### MkDocs with GitLab Pages

- GitLab Pages domain: `opsindev.news` (owner @dnsmichi) (Namecheap nameservers)
- MkDocs with Material theme 
    - dnsmichi sponsors MkDocs Material Insiders; this is implemented in the CI/CD configuration
    - If not tokens are detected, the default MkDocs Material theme is used 

### Buttondown 

> Disabled after 2024-03. 

- [Settings](https://buttondown.email/settings)
- Sending from a custom domain `news@opsin.dev` to use a unique sender for better creditibility in the future. This is configured in Namecheap DNS as a reply-catchall and forwards to @dnsmichi's private gmail address, in case someone replies. (TODO: Create a dedicated mailbox in case)
- [RSS feed enabled](https://buttondown.email/opsindev.news/rss)
- UTM sources enabled in [settings](https://buttondown.email/settings)

Owner: `@dnsmichi`

### Publishing Automation


- `Settings > CI/CD > Variables` has `BUTTONDOWN_API_TOKEN` as masked variable key with the [Button down API key](https://buttondown.email/settings/programming) as value. 




