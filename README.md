# opsindev.news

Source for the https://opsindev.news newsletter.

> **NOTE**
> 
> Public archive since 2024-03. 
> I changed my focus areas and time commitments and will continue sharing my learnings on AI, Embedded DevSecOps and more on my personal blog [dnsmichi.at](https://dnsmichi.at/), [LinkedIn](https://www.linkedin.com/in/dnsmichi/) and [Bluesky](https://bsky.app/profile/dnsmichi.dev).

## Newsletter Process

Please see the [Contributing guide](https://opsindev.news/contributing) which includes details on contributing, publishing and the platforms used. 
